package kethas.esolang.parser.ast;

import kethas.esolang.lexer.Token;

/**
 * Created by Kethas on 17/04/2017.
 */
public class Empty extends AST {

    public Empty(Token token) {
        super(token);
    }

}
